import java.io.IOException;
import java.util.Scanner;
import java.io.File;// Biblioteca IO para importa o arquivo CSV

public class teste{
    public static void main(String [] args){
        File arquivo = new File("C:\\Users\\Lucas volg\\Desktop\\testeEffetive\\PENDENTE\\MODELO_ARQUIVO_PARA_IMPORTACAO_DEV.csv"); // RECEBENDO O diretorio do arquivo CSV
        
        Boolean moverArquivo = null; // CRIANDO A VARIAVEL PARA PODER MOVER OS ARQUIVOS 
        Scanner sc = null;
        MoverInvalido moverI = new MoverInvalido();
        MoverValidado moverV = new MoverValidado();
        
        try {
            // VALIDADO AS TABELAS EXCEL
            sc = new Scanner(arquivo);
            String var = sc.nextLine();
            String[] parts = null;
            do{
                parts = var.split(";");
                
            } while (var == null);
            
            // VALIDAÇÃO DAS PLANILHAS

            if (parts.length == 4) {
                System.out.println("=== Arquivo validado ===");

                while (sc.hasNextLine()) {
                    String linha = sc.nextLine();
                    System.out.println(linha);
                }// .hasNEXTLine vai retorna verdadeiro se o programa não chegou no fim do arquivo
                System.out.println(" ");

                moverArquivo = true;
                
            } else {

                System.out.println("=== Arquivo Invalidado ===");
                moverArquivo = false;
            }
            
        }
        catch (IOException e){
             System.out.println(e);
        } // o IOException serve para todo tipo de entrada e saida do programa.
        finally {
            if (sc != null){
                sc.close();// fechando para evitar um vazamento de memoria na aplicação
            }

        }
        // MOVENDO OS ARQUIVOS CSV PARA SUAS PASTAS
        if (moverArquivo == true) {
            moverV.Mover();// chamando o metodo para mover o arquivo
        }
        else if (moverArquivo == false) {
            moverI.Mover();
        }
    }
}